﻿using UnityEngine;
using System.Collections;

public class zkRotate_TEST : MonoBehaviour {


    public float Xrpm = 0f;
    public float Yrpm = 10f;
    public float Zrpm = 0f;

    public bool complexRotation = true;
    public bool autoRoateEnable = true;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (autoRoateEnable)
        {
            if (complexRotation) transform.Rotate(Xrpm * Time.deltaTime, Yrpm * Time.deltaTime, Zrpm * Time.deltaTime);
            else
            {
                transform.Rotate(Vector3.right * Xrpm * Time.deltaTime);
                transform.Rotate(Vector3.up * Yrpm * Time.deltaTime);
                transform.Rotate(Vector3.forward * Zrpm * Time.deltaTime);

            }
        }
	}


    public void  autoRotate(bool state)
    {
        autoRoateEnable = state;
    }

    public  void  autoRotate(int state)
    {
        autoRoateEnable = (state != 0);
    }

    public void   autoRotate(float state)
    {
        autoRoateEnable = (state != 0);
    }

}
